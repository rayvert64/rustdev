// Primitive str = Immutable fixed-length strings somewhere in memory
// string = Growable, heap-allocated data struct. use when string will be modified


pub fn run() {
    let mut hello = String::from("Hell");

    println!("\n\"{}\" is {} characters long", hello, hello.len());

    // Push a single character on the end string
    hello.push('o');
    println!("\"{}\"is {} characters long", hello, hello.len());

    // Concatenate a string on string 
    hello.push_str(" World");
    println!("\"{}\" is {} characters long", hello, hello.len());

    // Get string capacity in bytes
    println!("{}", hello.capacity());

    
    // See if string contains substring
    println!("\"{}\" contains \"World\"? {}", hello, hello.contains("World"));


    // String replace
    println!("\"{}\" contains \"World\"? {}", hello.replace("World", "Phil"), hello.contains("World"));
    hello = hello.replace("World", "Phil");
    println!("\"{}\" contains \"World\"? {}", hello, hello.contains("World"));

    // Loop through string by whitespace
    for word in hello.split_whitespace() {
        println!("{}", word);
    }


    let mut s = String::with_capacity(10);
    s.push('a');
    s.push('b');

    // Assertion testing try changing 2 to 3; the program will crash
    assert_eq!(2, s.len());

    println!("{}", s);
    
}
