mod vars;
mod types;
mod strings;
mod tuples;
mod arrays;
mod vectors;

fn main() {
    vars::run();
    types::run();
    strings::run();
    tuples::run();
    arrays::run();
    vectors::run();
}
