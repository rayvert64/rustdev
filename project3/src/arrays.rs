// Arrays - Fixed lists where elements are the same data type

pub fn run() {
    // Create array of int of size 5
    let numbers: [i32; 5] = [1,2,3,4,5];

    // Print full array
    println!("\n{:?}", numbers);

    // Get single value
    println!("Elemement indexed at \"3\" is: {}", numbers[3]);

    // Get array length
    println!("Array length is: {}", numbers.len());

    // Arrays are stack allocated
    println!("Array occupies {} bytes", std::mem::size_of_val(&numbers));

    // Get slice of array
    let copy: &[i32] = &numbers;
    let slice: &[i32] = &numbers[1..3];
    println!("Full copy of string is: {:?}", copy);
    println!("Slice of string is: {:?}", slice);
}
