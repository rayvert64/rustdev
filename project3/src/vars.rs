/*
 * Variables hold primitive data or references to data.
 * Variables are immutable by default
 * Rust is a block-scoped language
 */
pub fn run () {
    // Define variables
    let name = "Phil";
    let mut age = 25; // This is now a mutable variable meaning we can change it
    age += 1;
    println!("My name is {}, and I am {}", name, age);


    // Define constants, type must be defined
    const ID: i32 = 001;
    println!("ID: {}", ID);


    // Assign mulpitple Variables
    let (my_name, my_age) = ("Phil", 26);
    println!("{} is {}", my_name, my_age);
}
