// Tuples group together values on different types
// Max 12 elements

pub fn run() {
    // Create tuple with: (string literal, string literal, 8bit int)
    let person: (&str, &str, i8) = ("Phil", "Balls", 26);

    println!("\n{} has huge {} and is {}", person.0, person.1, person.2);
}
