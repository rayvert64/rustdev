// Vectors - resizable vectors

pub fn run() {
    // Create vector of int of size 5
    let mut numbers: Vec<i32> = vec![1,2,3,4,5];

    // Print full vector
    println!("\n{:?}", numbers);

    // Get single value
    println!("Elemement indexed at \"3\" is: {}", numbers[3]);

    // Get vector length
    println!("vector length is: {}", numbers.len());

    // Vectors are stack allocated
    println!("Vector occupies {} bytes", std::mem::size_of_val(&numbers));

    // Get slice of vector
    let copy: &[i32] = &numbers;
    let slice: &[i32] = &numbers[1..3];
    println!("Full copy of vector is: {:?}", copy);
    println!("Slice of vector is: {:?}", slice);

    // Replace val 2 in vector
    numbers[2] = 33;
    println!("{:?}", numbers);

    // Push elemement to end of vector
    numbers.push(6);
    println!("{:?}", numbers);

    // Pop last value
    numbers.pop();
    println!("{:?}", numbers);

    // Loop through vector values
    for x in numbers.iter() {
        println!("Number: {}", x);
    }

    // Loop and mutate values
    for x in numbers.iter_mut() {
        *x *= 2;
    }
    println!("New values after iter loop: {:?}", numbers);

}
