mod print; // include print file

fn main() {
    // Call the run function from the print file
    print::run();

    
    // Basic formating
    println!("Printing: {}", 1);
    println!("{} is {}", "Phil", "Baller");

    
    // Positional arguments
    println!(
        "{0} is {1}, and {0} is {2}.", 
        "Phil", 
        "Baller", 
        "Awesome"
    );


    // Named arguments
    println!(
        "{name} likes to {activity}.",
        name = "Phil",
        activity = "code"
    );


    // Placeholder traits 
    println!(
        "\tDec: {}\n\tBinary: {:b}\n\tHex: {:x}\n\tOctal: {:o}", 
        13,
        13,
        13,
        13
    );


    /*
     * @important: Placeholder for debug trait 
     */
    println!(
        "Debug: {:?}",
        (12, true, "hello")
    );
}
